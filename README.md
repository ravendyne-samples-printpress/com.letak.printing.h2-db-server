## PrintPress Database Microservice

Used during development, this microservice simply starts H2 in-memory database server and loads initial data from JSON files.

### Dependencies

- [domain model](https://gitlab.com/ravendyne-samples-printpress/com.letak.printing.domain-model)
