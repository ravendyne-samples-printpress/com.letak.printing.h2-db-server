package com.letak.printing.h2db_server.repo;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {

}
