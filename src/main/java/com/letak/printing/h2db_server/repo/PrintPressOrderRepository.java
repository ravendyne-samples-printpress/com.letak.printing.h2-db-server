package com.letak.printing.h2db_server.repo;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.PrintPressOrder;

public interface PrintPressOrderRepository extends CrudRepository<PrintPressOrder, Integer> {

}
