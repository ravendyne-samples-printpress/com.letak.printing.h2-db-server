package com.letak.printing.h2db_server.repo;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.ProductParameter;

public interface ProductParameterRepository extends CrudRepository<ProductParameter, Integer> {

}
