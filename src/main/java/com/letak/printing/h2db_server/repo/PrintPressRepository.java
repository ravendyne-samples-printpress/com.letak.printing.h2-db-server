package com.letak.printing.h2db_server.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.PrintPress;

public interface PrintPressRepository extends CrudRepository<PrintPress, Integer> {

    List<PrintPress> findByUuid(String uuid);

}
