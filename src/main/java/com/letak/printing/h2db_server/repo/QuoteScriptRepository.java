package com.letak.printing.h2db_server.repo;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.QuoteScript;

public interface QuoteScriptRepository extends CrudRepository<QuoteScript, Integer> {

}
