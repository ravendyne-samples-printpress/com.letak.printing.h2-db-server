package com.letak.printing.h2db_server.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

import com.letak.printing.h2db_server.util.DataImporter;

@Component
public class ApplicationLifeCycle implements SmartLifecycle
{
    protected Log logger = LogFactory.getLog(ApplicationLifeCycle.class);
    
    @Autowired
    DataImporter dataLoader;
    
    private boolean running = false;

    @Override
    public boolean isRunning()
    {
        return running;
    }

    @Override
    public void start()
    {
        running = true;
        logger.debug("Lifecycle start");

        dataLoader.loadData();

        running = false;
    }

    @Override
    public void stop()
    {
        logger.debug("Lifecycle stop");
    }

    /**
     * Returning Integer.MAX_VALUE means we will be
     * the last bean to be called on startup and
     * the first one to be called on shutdown.
     */
    @Override
    public int getPhase()
    {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isAutoStartup()
    {
        return true;
    }

    @Override
    public void stop(Runnable callback)
    {
        logger.debug("Lifecycle stop with callback");
        callback.run();
    }

}
