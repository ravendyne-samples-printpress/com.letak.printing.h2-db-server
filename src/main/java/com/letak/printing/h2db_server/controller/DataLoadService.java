package com.letak.printing.h2db_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.letak.printing.h2db_server.util.DataImporter;

@RestController
@RequestMapping("/data")
public class DataLoadService {

    @Autowired
    DataImporter dataLoader;

    @RequestMapping("")
    public ServiceResponse index() {
        return new ServiceResponse("index");
    }

    @RequestMapping("load/{what}")
    public ServiceResponse load(@PathVariable("what") String what) {

        String message = "no data for '" + what + "'";

//        if( what.equals("templates") ) {
//            message = dataLoader.loadTemplates();
//        } else if( what.equals("test") ) {
//            message = dataLoader.loadTestData();
//        }

        return new ServiceResponse(message);
    }
}
