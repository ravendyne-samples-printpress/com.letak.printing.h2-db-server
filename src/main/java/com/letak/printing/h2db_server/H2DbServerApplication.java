package com.letak.printing.h2db_server;

import java.sql.SQLException;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EntityScan(basePackageClasses = {
        com.letak.printing.domain_model.Package.class, 
        com.letak.printing.h2db_server.repo.Package.class
        })
public class H2DbServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(H2DbServerApplication.class, args);
	}

	@Value("${spring.h2.server.tcp-port}")
	String tcpPort = "8043";

	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server inMemoryH2DatabaseaServer() throws SQLException {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", tcpPort);
	}
}
