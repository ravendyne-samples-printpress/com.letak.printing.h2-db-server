package com.letak.printing.h2db_server.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.letak.printing.domain_model.entity.PrintPressOrder;
import com.letak.printing.domain_model.entity.PrintPress;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductAttribute;
import com.letak.printing.domain_model.entity.ProductAttributeValue;
import com.letak.printing.domain_model.entity.ProductParameter;
import com.letak.printing.domain_model.entity.ProductParameterValue;
import com.letak.printing.domain_model.entity.QuoteScript;
import com.letak.printing.h2db_server.repo.PrintPressOrderRepository;
import com.letak.printing.h2db_server.repo.PrintPressRepository;

@Component
public class DataImporter {

    protected Log logger = LogFactory.getLog(DataImporter.class);
    
    private static final String NEW_LINE = "\n";

    private static final String PRINTPRESS_TEST_DATA_FILE = "/data/print_press.json";

    private static final String PRINTPRESS_TEMPLATES_DATA_FILE_PIAB = "/data/templates_piab.json";
    private static final String PRINTPRESS_TEMPLATES_DATA_FILE_PRINTAJ_BA = "/data/templates_printaj.ba.json";
    
    private static final String PRINTPRESS_DEMO_DATA_FILE_DEMO2 = "/data/demo2.json";

    private static final String DEMO_DATA_FILE_ORDERS = "/data/orders.json";

    @Autowired
    PrintPressRepository printPressRepository;
    
    @Autowired
    PrintPressOrderRepository orderRepository;
    
    public DataImporter() {}
    
    public String loadData() {

        StringBuilder message = new StringBuilder();
        
        message.append( loadData(PRINTPRESS_TEMPLATES_DATA_FILE_PIAB) );
        message.append( NEW_LINE );
        message.append( loadData(PRINTPRESS_TEMPLATES_DATA_FILE_PRINTAJ_BA) );
        message.append( NEW_LINE );
        message.append( loadData(PRINTPRESS_TEST_DATA_FILE) );
        message.append( NEW_LINE );
        message.append( loadData(PRINTPRESS_DEMO_DATA_FILE_DEMO2) );
        message.append( NEW_LINE );
        message.append( loadOrders(DEMO_DATA_FILE_ORDERS) );

        return message.toString();
    }

    private Object loadOrders(String dataFile) {

        ObjectMapper mapper = new ObjectMapper();

        TypeReference<List<PrintPressOrder>> mapType = new TypeReference<List<PrintPressOrder>>() {
        };

        InputStream is = TypeReference.class.getResourceAsStream(dataFile);

        String message;
        
        try {
            List<PrintPressOrder> orderList = mapper.readValue(is, mapType);
            
            for(PrintPressOrder order : orderList) {

                if(order.getPrintPress() == null) {
                    logger.warn("skipped importing order with no print perss set");
                    continue;
                }

                String printpressUuid = order.getPrintPress().getUuid();
                
                List<PrintPress> printPressList = printPressRepository.findByUuid(printpressUuid);
                
                if(printPressList.isEmpty()) {
                    logger.warn("skipped importing order with no print perss uuid set");
                    continue;
                }
                
                order.setPrintPress(printPressList.get(0));
            }
            
            orderRepository.saveAll(orderList);

            logger.debug("test data loaded from '" + dataFile + "'");

            message = "test data loaded successfully";

        } catch (IOException e) {
            
            logger.error("Couldn't load test data from '" + dataFile + "'", e);

            message = e.getMessage();
        }

        return message;
    }

    private Integer maxProductDisplayOrder;
    private Integer maxProductAttributeDisplayOrder;
    private Integer maxProductAttributeValueDisplayOrder;

    private String loadData(String dataFile) {
        ObjectMapper mapper = new ObjectMapper();

        TypeReference<List<PrintPress>> mapType = new TypeReference<List<PrintPress>>() {
        };

        InputStream is = TypeReference.class.getResourceAsStream(dataFile);

        String message;
        try {

            List<PrintPress> printpressList = mapper.readValue(is, mapType);

            // we have to set @ManyToOne column(s) in child rows before sending them to
            // CRUD repository interface for saving. Jackson will populate @OneToMany in
            // parent.
            for( PrintPress printpress : printpressList ) {
                
                List<Product> products = printpress.getProducts();
                if(products == null) continue;
                
                List<Product> productsToSetDisplayOrder = new ArrayList<Product>();
                maxProductDisplayOrder = 0;

                for( Product product : products ) {
                    product.setPrintPress(printpress);
                    
                    addForDisplayOrder( productsToSetDisplayOrder, product );
                    
                    List<ProductAttribute> prodAttrs = product.getProductAttributes();

                    if(prodAttrs != null) {

                        List<ProductAttribute> productAttributesToSetDisplayOrder = new ArrayList<ProductAttribute>();
                        maxProductAttributeDisplayOrder = 0;

                        for( ProductAttribute productAttribute : prodAttrs ) {
                            productAttribute.setProduct(product);

                            addForDisplayOrder( productAttributesToSetDisplayOrder, productAttribute );
                            
                            List<ProductAttributeValue> attrValues = productAttribute.getProductAttributeValues();
                            if(attrValues == null) continue;
                            
                            List<ProductAttributeValue> productAttributeValuesToSetDisplayOrder = new ArrayList<ProductAttributeValue>();
                            maxProductAttributeValueDisplayOrder = 0;

                            for( ProductAttributeValue value : attrValues ) {
                                value.setProductAttribute(productAttribute);
                                addForDisplayOrder( productAttributeValuesToSetDisplayOrder, value );
                            }
                            setAttributeValuesDisplayOrder( productAttributeValuesToSetDisplayOrder );
                        }
                        setAttributesDisplayOrder( productAttributesToSetDisplayOrder );
                    }
                    
                    
                    List<ProductParameter> prodParams = product.getProductParameters();

                    if(prodParams != null) {

                        for( ProductParameter param : prodParams ) {
                            param.setProduct(product);
                            List<ProductParameterValue> productParameterValues = param.getProductParameterValues();
                            if(productParameterValues != null) {
                                for( ProductParameterValue paramValue : productParameterValues ) {
                                    paramValue.setProductParameter(param);
                                }
                            }
                        }
                    }
                    
                    List<QuoteScript> prodScripts = product.getProductScripts();
                    
                    if( prodScripts != null ) {
                        for( QuoteScript script : prodScripts ) {
                            script.setProduct(product);
                        }
                    }

                }
                
                setProductsDisplayOrder( productsToSetDisplayOrder );
            }

            // now, foreign keys in child table(s) rows will be populated correctly
            printPressRepository.saveAll(printpressList);

            logger.debug("test data loaded from '" + dataFile + "'");

            message = "test data loaded successfully";

        } catch (IOException e) {
            
            logger.error("Couldn't load test data from '" + dataFile + "'", e);

            message = e.getMessage();
        }
        
        return message;
    }

    private void setAttributeValuesDisplayOrder(List<ProductAttributeValue> productAttributeValuesToSetDisplayOrder) {
        // set display order for product attribute values that doesn't have one
        for( ProductAttributeValue value : productAttributeValuesToSetDisplayOrder ) {
            maxProductAttributeValueDisplayOrder++;
            value.setDisplayOrder(maxProductAttributeValueDisplayOrder);
        }
    }

    private void addForDisplayOrder(List<ProductAttributeValue> productAttributeValuesToSetDisplayOrder, ProductAttributeValue value) {
        if( value.getDisplayOrder() == null ) {
            productAttributeValuesToSetDisplayOrder.add(value);
        } else {
            if( value.getDisplayOrder() > maxProductAttributeValueDisplayOrder ) {
                maxProductAttributeValueDisplayOrder = value.getDisplayOrder();
            }
        }
    }

    private void setAttributesDisplayOrder(List<ProductAttribute> productAttributesToSetDisplayOrder) {
        // set display order for product attributes that doesn't have one
        for( ProductAttribute attr : productAttributesToSetDisplayOrder ) {
            maxProductAttributeDisplayOrder++;
            attr.setDisplayOrder(maxProductAttributeDisplayOrder);
        }
    }

    private void addForDisplayOrder(List<ProductAttribute> productAttributesToSetDisplayOrder, ProductAttribute productAttribute) {
        if( productAttribute.getDisplayOrder() == null ) {
            productAttributesToSetDisplayOrder.add(productAttribute);
        } else {
            if( productAttribute.getDisplayOrder() > maxProductAttributeDisplayOrder ) {
                maxProductAttributeDisplayOrder = productAttribute.getDisplayOrder();
            }
        }
    }

    private void setProductsDisplayOrder(List<Product> productsToSetDisplayOrder) {
        // set display order for products that doesn't have one
        for( Product product : productsToSetDisplayOrder ) {
            maxProductDisplayOrder++;
            product.setDisplayOrder(maxProductDisplayOrder);
        }
    }

    private void addForDisplayOrder(List<Product> productsToSetDisplayOrder, Product product) {
        if( product.getDisplayOrder() == null ) {
            productsToSetDisplayOrder.add(product);
        } else {
            if( product.getDisplayOrder() > maxProductDisplayOrder ) {
                maxProductDisplayOrder = product.getDisplayOrder();
            }
        }
    }
    
}
